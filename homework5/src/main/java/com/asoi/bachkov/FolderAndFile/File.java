package com.asoi.bachkov.FolderAndFile;

public class File implements Name {
    private String fileName;
    private String fileType;

    public File(String fileName) {
        setName(fileName);
    }

    public void setName(String str) {
        int i = str.lastIndexOf('.');
        if (i > 0) {
            fileName = str.substring(0, i);
            fileType = str.substring(i);
        }
    }
}
