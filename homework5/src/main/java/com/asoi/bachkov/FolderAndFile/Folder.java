package com.asoi.bachkov.FolderAndFile;

import java.util.Map;
import java.util.HashMap;

public class Folder implements Name {
    private Map<String, Name> map = new HashMap<>();
    private String folderName;

    public Folder(String folderName) {
        this.folderName = folderName;
    }

    public void setName(String str) {
        folderName = str;
    }

    public Name addIn(String string) {
        Name name;
        if (!map.containsKey(string)) {
            if (string.lastIndexOf('.') != -1) {
                name = new File(string);
            } else {
                name = new Folder(string);
            }
            map.put(string, name);
        } else {
            name = map.get(string);
        }
        return name;
    }

    public String getStructureIn(int tab) {
        String result = "   ";
        String space = "";
        for (int i = 0; i < tab; i++) {
            space += "   ";
        }

        for (Map.Entry<String, Name> pair : map.entrySet()) {
            String key = pair.getKey();
            result += key + "\n" + space;
            if (pair.getValue() instanceof Folder) {
                result += ((Folder) pair.getValue()).getStructureIn(tab + 1);
            }
        }
        return result.substring(0, result.length() - 3);
    }
}
