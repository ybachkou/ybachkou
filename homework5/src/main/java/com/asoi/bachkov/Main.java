package com.asoi.bachkov;
import com.asoi.bachkov.FolderAndFile.Folder;
import com.asoi.bachkov.Structures.StructureForDisplayOutput;
import com.asoi.bachkov.Structures.StructureForUserInput;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Folder folder = new Folder("");
        for (int i = 0; ; i++) {
            String line = new Scanner(System.in).nextLine();
            if (line.equals("print")) {
                StructureForDisplayOutput.structureToString(folder);
                break;
            }
            StructureForUserInput.addToStructure(folder, line);
        }
    }
}