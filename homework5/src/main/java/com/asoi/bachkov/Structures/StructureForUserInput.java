package com.asoi.bachkov.Structures;

import com.asoi.bachkov.FolderAndFile.Folder;

public class StructureForUserInput {
    public static void addToStructure(Folder folder, String... strings) {
        StructureFromString builder = new StructureFromString(folder);
        for (String string : strings) {
            builder.addToStructure(string);
        }
    }
}
