package com.asoi.bachkov.Structures;
import com.asoi.bachkov.FolderAndFile.Folder;
import com.asoi.bachkov.FolderAndFile.Name;

public class StructureFromString {
    private Folder folder;

    public StructureFromString(Folder folder) {
        this.folder = folder;
    }

    public void addToStructure(String str) {
        String[] objects = str.split("/");
        Folder currentFolder = folder;
        for (String object : objects) {
            Name name = currentFolder.addIn(object);
            if (name instanceof Folder) {
                currentFolder = (Folder) name;
            } else {
                break;
            }

        }
    }
}
