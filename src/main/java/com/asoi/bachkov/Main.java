package com.asoi.bachkov;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Task 2:");
        task2();
    }

    public static void task2() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("Input 3 integers: ");
            int algorithmId = sc.nextInt();
            int loopType = sc.nextInt();
            int n = sc.nextInt();
            if (loopType < 1 || loopType > 3 || n < 0) {
                System.err.println("Something went wrong. Try again.");
            } else {
                if (algorithmId == 1) {
                    fibonacciNumber(loopType, n);
                } else if (algorithmId == 2) {
                    factorial(loopType, n);
                } else {
                    System.err.println("Something went wrong. Try again.");
                }
            }
            sc.close();
        } catch (Exception e) {
            System.err.println("Something went wrong. Try again.");
        }
    }

    public static void fibonacciNumber(int loopType, int n) {
        int one = 0;
        int two = 1;
        int newNumber;
        if (n == 0) {
            System.out.print("Fibonacci sequence: ");
        } else if (n == 1) {
            System.out.print("Fibonacci sequence: " + one);
        } else {
            System.out.print("Fibonacci sequence: " + one + " " + two + " ");
        }
        if (loopType == 1) {
            while (n - 2 > 0) {
                n--;
                newNumber = one + two;
                one = two;
                two = newNumber;
                System.out.print(newNumber + " ");
            }
        } else if (loopType == 2) {
            do {
                n--;
                newNumber = one + two;
                one = two;
                two = newNumber;
                System.out.print(newNumber + " ");

            } while (n - 2 > 0);
        } else if (loopType == 3) {
            for (int i = 0; i < n - 2; i++) {
                newNumber = one + two;
                one = two;
                two = newNumber;
                System.out.print(newNumber + " ");
            }
        }
    }

    public static void factorial(int loopType, int n) {
        int fact = 1;
        int k = 1;
        if (loopType == 1) {
            while (n - 1 > 0) {
                n--;
                k++;
                fact = fact * k;
            }
        } else if (loopType == 2) {
            do {
                n--;
                k++;
                fact = fact * k;
            } while (n - 1 > 0);

        } else if (loopType == 3) {
            for (int i = 1; i <= n; i++) {
                fact = fact * i;
            }
        } else {
            System.err.println("Something went wrong. Try again.");
        }
        System.out.print("Factorial of number n = " + fact);
    }
}